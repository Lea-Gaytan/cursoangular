(()=>{
    const avenger ={
        nombre: 'Steve',
        clave:  'Capitan America',
        poder:  'Droga'

    }

    const extraer = (avenger: any) => {

    //se pueden extraer cada una de las propiedades del objeto y crear variables inmediatamente
    const { nombre, clave} = avenger;


    console.log(avenger.nombre);
    console.log(avenger.clave);
    console.log(avenger.poder);
    }

    extraer( avenger );

    const extraer2 = ({nombre,poder}: any) => {
    
        console.log(avenger.nombre);
        console.log(avenger.clave);
   
        }
    
        extraer( avenger );
        extraer2(avenger);

        //Desestructuraciòn de arreglos

        const avengers : string[] = ['Thor','Ironman','Spider'];

        //forma tradicional
        console.log(avengers[0]);
        console.log(avengers[1]);
        console.log(avengers[2]);

        //desestructurando el arreglo
        const [loki, ironman, arania] = avengers;
        //const [, , arania] = avengers;

        console.log(loki);
        console.log(ironman);
        console.log(arania);
        




})();