"use strict";
(function () {
    var avenger = {
        nombre: 'Steve',
        clave: 'Capitan America',
        poder: 'Droga'
    };
    var extraer = function (avenger) {
        //se pueden extraer cada una de las propiedades del objeto y crear variables inmediatamente
        var nombre = avenger.nombre, clave = avenger.clave;
        console.log(avenger.nombre);
        console.log(avenger.clave);
        console.log(avenger.poder);
    };
    extraer(avenger);
    var extraer2 = function (_a) {
        var nombre = _a.nombre, poder = _a.poder;
        console.log(avenger.nombre);
        console.log(avenger.clave);
    };
    extraer(avenger);
    extraer2(avenger);
    //Desestructuraciòn de arreglos
    var avengers = ['Thor', 'Ironman', 'Spider'];
    //forma tradicional
    console.log(avengers[0]);
    console.log(avengers[1]);
    console.log(avengers[2]);
    //desestructurando el arreglo
    var loki = avengers[0], ironman = avengers[1], arania = avengers[2];
    //const [, , arania] = avengers;
    console.log(loki);
    console.log(ironman);
    console.log(arania);
})();
