(function () {
    var enviarMision = function (xmen) {
        console.log("Enviando a " + xmen.nombre + " a la misi\u00F3n");
    };
    var regresarAlCuartel = function (xmen) {
        console.log("Enviando al " + xmen.nombre + " cuartel");
    };
    var wolverine = {
        nombre: 'Logan',
        edad: 60
    };
    var mistica = {
        nombre: 'Jenifer',
        edad: 30
    };
    var magneto = {
        nombre: 'James',
        edad: 50
    };
    enviarMision(wolverine);
    regresarAlCuartel(wolverine);
    enviarMision(mistica);
    regresarAlCuartel(mistica);
    enviarMision(magneto);
    regresarAlCuartel(magneto);
    ////tipado del retorno de una funcion
    var sumar = function (a, b) { return a + b; };
    var nombre = function () { return 'Loreley Legasphi'; };
    //ejemplo donde typescript no puede inferir el tipo
    var obtenerSalario = function () {
        return new Promise(function (resolve, reject) {
            resolve('Fernando');
        });
    };
    obtenerSalario().then(function (a) { return console.log(a.toUpperCase()); });
})();
