
(()=>{
   
    //en una interfaz no se define lo que va hacer, no tiene constructores,
    //unicamente son las reglas que quiere que cumpla el objeto
    //las interfaces no se reflejan en js
    interface Xmen {
        nombre: string;
        edad: number;
        //se pone el signo de ? para indicar que ese atributo es opcional
        poder?: string;
    }
    
    const enviarMision = ( xmen: Xmen ) => {
        console.log(`Enviando a ${ xmen.nombre } a la misión`);
    }

    const regresarAlCuartel = ( xmen: Xmen ) => {
        console.log(`Enviando al ${ xmen.nombre } cuartel`);
    }


    const wolverine: Xmen = {
        nombre: 'Logan',
        edad: 60
    }

    const mistica: Xmen = {
        nombre: 'Jenifer',
        edad: 30
    }

    const magneto: Xmen = {
        nombre: 'James',
        edad: 50
    }

    enviarMision( wolverine );
    regresarAlCuartel( wolverine );

    enviarMision( mistica );
    regresarAlCuartel( mistica );

    enviarMision( magneto );
    regresarAlCuartel( magneto );

    ////tipado del retorno de una funcion
    const sumar = ( a:number, b:number ) : number => a+b;

    const nombre = (): string =>'Loreley Legasphi';

    //ejemplo donde typescript no puede inferir el tipo

    const obtenerSalario= () : Promise<string> => {

        return new Promise((resolve, reject ) => {
            resolve('Fernando');

        });
    };


    obtenerSalario().then(a => console.log( a.toUpperCase()))
})();