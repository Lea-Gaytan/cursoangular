(function () {
    var Avenger = /** @class */ (function () {
        function Avenger(nombre, equipo, nombreReal, puedePelear, peleasGanadas) {
            this.nombre = nombre;
            this.equipo = equipo;
            this.nombreReal = nombreReal;
            this.puedePelear = puedePelear;
            this.peleasGanadas = peleasGanadas;
        }
        return Avenger;
    }());
    var Xmen = /** @class */ (function () {
        // nombre: string;
        // equipo: string;
        // nombreReal: string;
        // puedePelear: boolean;
        // peleasGanadas: number;
        //forma mas sencilla de hacer la declaración de las propiedades e inicializarlas de una vez de la siguiente manera
        function Xmen(nombre, equipo, nombreReal, puedePelear, peleasGanadas) {
            if (puedePelear === void 0) { puedePelear = true; }
            if (peleasGanadas === void 0) { peleasGanadas = 0; }
            this.nombre = nombre;
            this.equipo = equipo;
            this.nombreReal = nombreReal;
            this.puedePelear = puedePelear;
            this.peleasGanadas = peleasGanadas;
        }
        return Xmen;
    }());
    var antman = new Avenger('Antman', 'Capi', 'Scott', true, 6);
    var magneto = new Xmen('Magneto', 'xmen');
    console.log(antman);
    console.log(magneto);
})();
