
(()=>{
   
    class Avenger {

         nombre: string;
         equipo: string;
         nombreReal: string;

         puedePelear: boolean;
         peleasGanadas?: number;

         constructor( nombre: string, equipo: string, nombreReal:string, puedePelear:boolean, peleasGanadas:number ){
             this.nombre = nombre;
             this.equipo = equipo;
             this.nombreReal = nombreReal;
             this.puedePelear = puedePelear;
             this.peleasGanadas = peleasGanadas;
         }
        
 }

    class Xmen {

        // nombre: string;
        // equipo: string;
        // nombreReal: string;

        // puedePelear: boolean;
        // peleasGanadas: number;

    //forma mas sencilla de hacer la declaración de las propiedades e inicializarlas de una vez de la siguiente manera
        constructor( public nombre: string,
                     public equipo: string,
                     public nombreReal?: string,
                     public puedePelear: boolean  = true,
                     public peleasGanadas: number = 0){}
        
    }

    const antman = new Avenger('Antman', 'Capi', 'Scott', true, 6);
    const magneto = new Xmen('Magneto','xmen');
    
    console.log(antman);
    console.log(magneto);

})();