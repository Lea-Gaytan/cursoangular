(function(){

    //funcion asignada a una variable
    let miFuncion = function(a: string ){
      return a.toUpperCase();
      }
    //funcion tradicional
    function funcion2( a: string ){
        return a;
    }

    //funcion de flecha, esmas sencilla y mas compacta
    const miFuncionF = (a: string ) => a.toUpperCase();

    //funcion normal
    const sumarN = function( a:number, b:number){
        return a+b;
    }

    //ejercicio funcion flecha

    const sumarF = (a:number, b:number ) => a+b;

    const hulk ={
        nombre: 'Hulk',
        smash(){
            //para ejecutar un segundo despues
            setTimeout(() => {
                console.log(`${ this.nombre} smash!!!`);

            },1000);
        }
        
    }


    console.log(miFuncion('Normal'));
    console.log(miFuncion('Flecha'));
    console.log(sumarN(10,4));
    console.log(sumarF(20,3));

    hulk.smash();




})();