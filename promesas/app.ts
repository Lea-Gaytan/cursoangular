
(()=>{
   
    console.log('Inicio');

    //"target": "es6",  incorporó las promesas por eso debe cambiarse en el archivo .json
    //resolve;lo que retorna cuando todo funciona correctamente
    //reject; funcion a la que se llama si sucede algun error
    //parecido con una funcion asincrona, o cuando necesitas ejecutar muchas cosas y esperar a que terminen para llamar otra

    const prom1 = new Promise(( resolve, reject  ) => {

        setTimeout(() => {
            reject('Se terminó el timeout');
        }, 1000);


    });


    prom1
        .then( mensaje => console.log( mensaje ))//lo que quiero ejecutar cuando se realiza todo correctamente
        .catch( err => console.warn( err ));//lo que quiero ejecutar cuando sucede un error

    console.log('Fin');




    const retirarDinero = ( montoRetirar: number ): Promise<number> => {

        let dineroActual = 1000;

        return new Promise( (resolve, reject) => {

            if ( montoRetirar > dineroActual  ) {
                reject('No hay suficientes fondos');
            } else {
                dineroActual -= montoRetirar;
                resolve( dineroActual );
            }

        });

    }



    retirarDinero( 50)
        .then( montoActual => console.log(`Me queda ${ montoActual }`) )
        .catch( console.warn )
})();