(function () {
    console.log('Inicio');
    //"target": "es6",  incorporó las promesas por eso debe cambiarse en el archivo .json
    //resolve;lo que retorna cuando todo funciona correctamente
    //reject; funcion a la que se llama si sucede algun error
    //parecido con una funcion asincrona, o cuando necesitas ejecutar muchas cosas y esperar a que terminen para llamar otra
    var prom1 = new Promise(function (resolve, reject) {
        setTimeout(function () {
            reject('Se terminó el timeout');
        }, 1000);
    });
    prom1
        .then(function (mensaje) { return console.log(mensaje); }) //lo que quiero ejecutar cuando se realiza todo correctamente
    ["catch"](function (err) { return console.warn(err); }); //lo que quiero ejecutar cuando sucede un error
    console.log('Fin');
    var retirarDinero = function (montoRetirar) {
        var dineroActual = 1000;
        return new Promise(function (resolve, reject) {
            if (montoRetirar > dineroActual) {
                reject('No hay suficientes fondos');
            }
            else {
                dineroActual -= montoRetirar;
                resolve(dineroActual);
            }
        });
    };
    retirarDinero(500)
        .then(function (montoActual) { return console.log("Me queda " + montoActual); })["catch"](console.warn);
})();
