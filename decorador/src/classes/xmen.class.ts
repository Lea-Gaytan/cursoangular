//funcion que recibe una funcion
function imprimirConsola(constructorClase: Function) {
    
    console.log(constructorClase);


}

//el decorador se coloca antes de la declaración de la clase, nos sirve para añadirle funcionalidades a la clase
//modificar tsconfig.json descomentar --> "experimentalDecorators": true, para utilizar el decorador

//exportar la clase para indicar que esta clase puede ser utilizada en otro archivo mediante la importacion
//si quiere que continue como privada no se le pone export

@imprimirConsola
export class Xmen {

    constructor(
        public nombre: string,
        public clave: string
    ){}

    imprimir(){
    console.log(`Impresión desde el metodo imprimir ------> ${this.nombre} - ${this.clave}`);

    }


} 

