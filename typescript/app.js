(function () {
    var mensaje = 'Hola';
    var numero = 123;
    var booleano = true;
    var hoy = new Date();
    var cualquiercosa;
    //puede adquirir cualquier valor
    cualquiercosa = mensaje;
    cualquiercosa = numero;
    cualquiercosa = booleano;
    cualquiercosa = hoy;
    var spiderman = {
        nombre: 'Peter',
        edad: 30
    };
    //al crear un nuevo objeto, se le deben agregar las propiedades definidas
    spiderman = {
        nombre: 'Juan',
        edad: 15
    };
})();
