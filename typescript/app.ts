(function(){

    let mensaje: string = 'Hola';
    let numero: number = 123;
    let booleano: boolean = true;
    let hoy: Date = new Date();

    let cualquiercosa : string | number | boolean | Date;
    //puede adquirir cualquier valor
    cualquiercosa = mensaje;
    cualquiercosa = numero;
    cualquiercosa = booleano;
    cualquiercosa = hoy;


    let spiderman = {
        nombre:'Peter',
        edad:30
    };

    //al crear un nuevo objeto, se le deben agregar las propiedades definidas
    spiderman = {
        nombre:'Juan',
        edad:15
    };

})();