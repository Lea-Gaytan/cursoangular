//exportar la clase para indicar que esta clase puede ser utilizada en otro archivo mediante la importacion
//si quiere que continue como privada no se le pone export

export class Xmen {

    constructor(
        public nombre: string,
        public clave: string
    ){}

    imprimir(){
    console.log(`Impresión desde el metodo imprimir -> ${this.nombre} - ${this.clave}`);

    }
}

