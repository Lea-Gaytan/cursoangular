(function () {
    //funcion asignada a una variable
    var miFuncion = function (a) {
        return a.toUpperCase();
    };
    //funcion tradicional
    function funcion2(a) {
        return a;
    }
    //funcion de flecha, esmas sencilla y mas compacta
    var miFuncionF = function (a) { return a.toUpperCase(); };
    //funcion normal
    var sumarN = function (a, b) {
        return a + b;
    };
    //ejercicio funcion flecha
    var sumarF = function (a, b) { return a + b; };
    var hulk = {
        nombre: 'Hulk',
        smash: function () {
            console.log(this.nombre + " smash!!!");
        }
    };
    console.log(miFuncion('Normal'));
    console.log(miFuncion('Flecha'));
    console.log(sumarN(10, 4));
    console.log(sumarF(20, 3));
    hulk.smash();
})();
