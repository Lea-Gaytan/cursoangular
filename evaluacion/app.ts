(()=>{

    // Uso de Let y Const
   // var nombre = 'Ricardo Tapia';
    const nombre: string = 'Ricardo Tapia';
    //var edad = 23;
    const edad: number = 23;

  
   // var PERSONAJE = {
     // nombre: nombre,
      //edad: edad
    //};
    const PERSONAJE = {nombre, edad}

  
    // Cree una interfaz que sirva para validar el siguiente objeto
    //var batman = {
    //  nombre: 'Bruno Díaz',
    //  artesMarciales: ['Karate','Aikido','Wing Chun','Jiu-Jitsu']
    //}
  
    interface SuperHeroe {
        nombre: string;
        artesMarciales : string[];
    }

    const batman: SuperHeroe = {
        nombre: 'Bruno Díaz',
        artesMarciales: ['Karate','Aikido','Wing Chun','Jiu-Jitsu']
    }

    // Convertir esta funcion a una funcion de flecha
    //function resultadoDoble( a, b ){
    //  return (a + b) * 2
    //}

    const resultadoDoble = (a:number, b:number ) => {
        return ((a+b)*2)
    }
  
    // Función con parametros obligatorios, opcionales y por defecto
    // donde NOMBRE = obligatorio
    //       PODER  = opcional
    //       ARMA   = por defecto = 'arco'
    
    //function getAvenger( nombre, poder, arma ){
    //  var mensaje;
    //  if( poder ){
     //   mensaje = nombre + ' tiene el poder de: ' + poder + ' y un arma: ' + arma;
    //  }else{
      //  mensaje = nombre + ' tiene un ' + poder
     // }
   // };


    function getAvenger(
        nombre   : string,
        poder?: string,
        arma  : string = 'arco'){

        if ( poder ) {
            console.log(`${ nombre } tiene el poder de: ${ poder } y un arma ${ arma } .`);
        } else {
            console.log(`${ nombre } tiene un ${ poder }.`);
        }
    }
    getAvenger('Guasón', 'maldad');
  
    // Cree una clase que permita manejar la siguiente estructura
    // La clase se debe de llamar rectangulo,
    // debe de tener dos propiedades:
    //   * base
    //   * altura
    // También un método que calcule el área  =  base * altura,
    // ese método debe de retornar un numero.

    class Rectangulo {
        constructor( public base: number,
                     public altura: number,
                   ){}

        calcularArea(){
            return this.base*this.altura;
        }
        
    }
  
  
  
  })();